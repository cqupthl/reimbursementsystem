package com.example.reimbursementsystem.service.impl;

import com.example.reimbursementsystem.service.EmployeeService;
import com.example.reimbursementsystem.tiny.mbg.mapper.EmployeeMapper;
import com.example.reimbursementsystem.tiny.mbg.model.Employee;
import com.example.reimbursementsystem.tiny.mbg.model.EmployeeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName EmployeeServiceImpk
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/28
 * @Version V1.0
 **/
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeMapper employeeMapper;

    @Override
    public int add(Employee employee) {
       return  employeeMapper.insert(employee);
    }

    @Override
    public int delete(String id) {
      return  employeeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Employee employee) {
        return employeeMapper.updateByPrimaryKeySelective(employee);
    }

    @Override
    public Employee selectEmployeById(String id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Employee> selectAll() {
        return employeeMapper.selectByExample(new EmployeeExample());
    }

    @Override
    public String getEmployeeName(String id) {
        Employee employee=employeeMapper.selectByPrimaryKey(id);
        return employee.getName();
    }
}
