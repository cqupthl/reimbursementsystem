package com.example.reimbursementsystem.service.impl;

import com.example.reimbursementsystem.service.DepartmentService;
import com.example.reimbursementsystem.tiny.mbg.mapper.DepartmentMapper;
import com.example.reimbursementsystem.tiny.mbg.model.Department;
import com.example.reimbursementsystem.tiny.mbg.model.DepartmentExample;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @ClassName DepartmentService
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/29
 * @Version V1.0
 **/
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    DepartmentMapper departmentMapper;

    @Override
    public int addDepartment(Department department) {
        return departmentMapper.insert(department);
    }

    @Override
    public int deleteDepartment(String sn) {
       return departmentMapper.deleteByPrimaryKey(sn);
    }

    @Override
    public int updateDepartment(Department department) {
        return departmentMapper.updateByPrimaryKey(department);
    }

    @Override
    public List<Department> getAll() {
        return departmentMapper.selectByExample(new DepartmentExample());
    }

    @Override
    public Department getDepartmentById(String id) {
        return departmentMapper.selectByPrimaryKey(id);
    }
}
