package com.example.reimbursementsystem.service;

import com.example.reimbursementsystem.tiny.mbg.model.Department;

import java.util.List;

public interface DepartmentService {
    int addDepartment(Department department);
    int deleteDepartment(String sn);
    int updateDepartment(Department department);
    List<Department> getAll();
    Department getDepartmentById(String  id);
}
