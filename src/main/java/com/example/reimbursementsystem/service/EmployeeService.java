package com.example.reimbursementsystem.service;

import com.example.reimbursementsystem.tiny.mbg.model.Employee;

import java.util.List;

public interface EmployeeService {
     int  add(Employee employee);
     int  delete(String id);
     int  update(Employee employee);
     Employee selectEmployeById(String id);
     List<Employee> selectAll();
     String getEmployeeName(String id);
}
