package com.example.reimbursementsystem.tiny.mbg.mapper;

import com.example.reimbursementsystem.tiny.mbg.model.ClaimVoucher;
import com.example.reimbursementsystem.tiny.mbg.model.ClaimVoucherExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ClaimVoucherMapper {
    int countByExample(ClaimVoucherExample example);

    int deleteByExample(ClaimVoucherExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ClaimVoucher record);

    int insertSelective(ClaimVoucher record);

    List<ClaimVoucher> selectByExample(ClaimVoucherExample example);

    ClaimVoucher selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ClaimVoucher record, @Param("example") ClaimVoucherExample example);

    int updateByExample(@Param("record") ClaimVoucher record, @Param("example") ClaimVoucherExample example);

    int updateByPrimaryKeySelective(ClaimVoucher record);

    int updateByPrimaryKey(ClaimVoucher record);
}