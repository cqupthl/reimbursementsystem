package com.example.reimbursementsystem.tiny.mbg.mapper;

import com.example.reimbursementsystem.tiny.mbg.model.DealRecord;
import com.example.reimbursementsystem.tiny.mbg.model.DealRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DealRecordMapper {
    int countByExample(DealRecordExample example);

    int deleteByExample(DealRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DealRecord record);

    int insertSelective(DealRecord record);

    List<DealRecord> selectByExample(DealRecordExample example);

    DealRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DealRecord record, @Param("example") DealRecordExample example);

    int updateByExample(@Param("record") DealRecord record, @Param("example") DealRecordExample example);

    int updateByPrimaryKeySelective(DealRecord record);

    int updateByPrimaryKey(DealRecord record);
}