package com.example.reimbursementsystem.tiny.mbg.mapper;

import com.example.reimbursementsystem.tiny.mbg.model.ClaimVoucherItem;
import com.example.reimbursementsystem.tiny.mbg.model.ClaimVoucherItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ClaimVoucherItemMapper {
    int countByExample(ClaimVoucherItemExample example);

    int deleteByExample(ClaimVoucherItemExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ClaimVoucherItem record);

    int insertSelective(ClaimVoucherItem record);

    List<ClaimVoucherItem> selectByExample(ClaimVoucherItemExample example);

    ClaimVoucherItem selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ClaimVoucherItem record, @Param("example") ClaimVoucherItemExample example);

    int updateByExample(@Param("record") ClaimVoucherItem record, @Param("example") ClaimVoucherItemExample example);

    int updateByPrimaryKeySelective(ClaimVoucherItem record);

    int updateByPrimaryKey(ClaimVoucherItem record);
}