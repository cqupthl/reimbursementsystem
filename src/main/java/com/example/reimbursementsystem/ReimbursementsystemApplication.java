package com.example.reimbursementsystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.reimbursementsystem.tiny.mbg.mapper")
public class ReimbursementsystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReimbursementsystemApplication.class, args);
    }

}
