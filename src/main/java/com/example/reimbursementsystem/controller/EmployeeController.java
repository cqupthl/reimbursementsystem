package com.example.reimbursementsystem.controller;

import com.example.reimbursementsystem.service.EmployeeService;
import com.example.reimbursementsystem.service.DepartmentService;
import com.example.reimbursementsystem.tiny.common.api.CommonResult;
import com.example.reimbursementsystem.tiny.mbg.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName EmployeeControlller
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/28
 * @Version V1.0
 **/
@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @Autowired
    DepartmentService departmentService;

    @GetMapping("/selectAll")
    @ResponseBody
    public CommonResult<List<Employee>> selectAll() {
        List<Employee> res=employeeService.selectAll();
        return CommonResult.success(res);
    }

    @PostMapping("/addemployee")
    @ResponseBody
    public CommonResult addEmployee(@RequestParam Employee employee){
        CommonResult commonResult;
         int flag=employeeService.add(employee);
         if(flag==1){
             commonResult=CommonResult.success("添加成功");
         }else{
             commonResult=CommonResult.failed("添加失败");
         }
         return commonResult;
    }

    //根据id删除员工信息
    @PostMapping("/deleteemployee")
    @ResponseBody
    public CommonResult deleteEmployee(@RequestParam String id ){
        return null;
    }






}
