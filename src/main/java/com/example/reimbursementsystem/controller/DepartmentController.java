package com.example.reimbursementsystem.controller;

import com.example.reimbursementsystem.service.DepartmentService;
import com.example.reimbursementsystem.tiny.common.api.CommonResult;
import com.example.reimbursementsystem.tiny.mbg.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName DepartmentController
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/30
 * @Version V1.0
 **/
@Controller
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @RequestMapping(method= RequestMethod.GET,path = "/getAllDepartment")
    @ResponseBody
    public CommonResult<List<Department>> getAllDepartment(){
       return CommonResult.success(departmentService.getAll());
    }

    @PostMapping("/addDeaprtment")
    @ResponseBody
    public CommonResult addDepartment(Department department){
        CommonResult commonResult;
       int flag=departmentService.addDepartment(department);
       if(flag==1){
           commonResult=CommonResult.success("添加成功");
       }else{
           commonResult=CommonResult.failed("添加失败");
       }
       return commonResult;
    }

    @GetMapping("/getDepartmentById")
    @ResponseBody
    public CommonResult<Department> getDepartmentById(String sn){
        return CommonResult.success(departmentService.getDepartmentById(sn));
    }

    @PostMapping("/deleteDepartmentById")
    @ResponseBody
    public CommonResult deleteDepartmentById(String sn){
        CommonResult commonResult;
        return null;
    }




}
